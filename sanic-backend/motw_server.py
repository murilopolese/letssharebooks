import rapidjson as rjson
import psutil
import os
import sys
import time
import re
import zlib
import pickle
import pathlib
from signal import signal, SIGINT
from asgiref.sync import sync_to_async
import sortedcontainers
from setproctitle import setproctitle
from urllib.parse import unquote_plus as unq

from sanic import Sanic
from sanic import response as rs
from sanic.response import json
from sanic.response import text
from sanic.views import HTTPMethodView
from sanic.views import stream as stream_decorator
from sanic.exceptions import abort
from sanic_cors import CORS

import motw
import asyncio
import uvloop

setproctitle("sanic_motw")


app = Sanic()
CORS(app)
app.config.REQUEST_MAX_SIZE = 1000000000

# - load libraries if they persits
if not pathlib.Path("unitedstates").exists():
    pathlib.Path("unitedstates").mkdir(parents=True, exist_ok=True)

motw.libraries = motw.load_libraries()

# - SANIC/rapidjson


def add_library_url(book):
    book.update({"library_url": motw.libraries[book["library_uuid"]]["url"]})
    return book


def hateoas(l, request, title="books"):
    max_results = motw.hateoas.max_results
    if "max_results" in request.args:
        try:
            max_results = min(120, abs(int(request.args["max_results"][0])))
        except Exception as e:
            print("messy max_results was sent: {}".format(e))

    r = {}
    try:
        p = int(request.args["page"][0]) - 1
    except:
        p = 0

    last_p = int(len(l) / max_results + 1)

    if p > last_p:
        p = last_p
    elif p < 0:
        p = 0

    books = [
        {
            k: v
            for (k, v) in b.items()
            if k
            in [
                "_id",
                "authors",
                "pubdate",
                "title",
                "formats",
                "library_uuid",
                "cover_url",
            ]
        }
        for b in l[p * max_results : (p + 1) * max_results]
    ]
    r = {
        "_items": [add_library_url(book) for book in books],
        "_links": {
            "parent": {"title": "Memory of the World Library", "href": "/"},
            "self": {"title": title, "href": request.path[1:]},
            "prev": {
                "title": "previous page",
                "href": "{}?page={}".format(request.path, p),
            },
            "next": {
                "title": "next page",
                "href": "{}?page={}".format(request.path, p + 2),
            },
            "last": {
                "title": "last page",
                "href": "{}?page={}".format(request.path, last_p),
            },
        },
        "_meta": {
            "page": p + 1,
            "max_results": max_results,
            "total": len(l),
            "status": title,
        },
    }

    if p == 0:
        del r["_links"]["prev"]
    if p == last_p - 1:
        del r["_links"]["next"]

    return rjson.dumps(r, datetime_mode=rjson.DM_ISO8601).encode()


def check_library_secret(library_uuid, library_secret):
    secret = motw.load_libraries()[library_uuid]["secret"]
    ret = (
        True
        if (library_secret == motw.master_secret or library_secret == secret)
        else False
    )
    if ret:
        return ret
    else:
        abort(403)


@sync_to_async
def validate_books(bookson, schema, enc_zlib):
    if enc_zlib:
        try:
            bookson = zlib.decompress(bookson).decode("utf-8")
        except zlib.error as e:
            abort(422, "Unzipping JSON failed...")

    validate = rjson.Validator(rjson.dumps(schema))
    try:
        validate(bookson)
        return bookson
    except ValueError as e:
        print(e)
        abort(422, "JSON didn't validate.")


@sync_to_async
def remove_books(rookson, library_uuid):
    pickled_books = sortedcontainers.SortedDict()
    bookids = rjson.loads(rookson)
    if bookids == []:
        return True
    # t = time.time()
    with (open("unitedstates/{}".format(library_uuid), "rb")) as f:
        pickled_books.update(pickle.load(f))

    for bookid in bookids:
        pickled_books.pop(bookid, None)

    with open("unitedstates/{}".format(library_uuid), "wb") as f:
        pickle.dump(pickled_books, f)

    # print("books removed in {} seconds.".format(round(time.time() - t, 3)))
    return True


@sync_to_async
def add_books(bookson, library_uuid):
    pickled_books = sortedcontainers.SortedDict()
    t = time.time()
    books = rjson.loads(bookson, datetime_mode=rjson.DM_ISO8601)
    print("books.json loaded in {} seconds.".format(round(time.time() - t, 3)))
    if books == []:
        return True
    library_uuid_check = list(set([book["library_uuid"] for book in books]))
    if len(library_uuid_check) != 1 or library_uuid_check[0] != library_uuid:
        return False

    new_book_ids = set((book["_id"] for book in books))
    t = time.time()
    with (open("unitedstates/{}".format(library_uuid), "rb")) as f:
        pickled_books.update(pickle.load(f))
        old_books_ids = pickled_books.keys()
    print("pickled_books loaded in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    ids_to_add = set(new_book_ids - old_books_ids)
    for b in books:
        if b["_id"] not in ids_to_add:
            continue

        if "series" not in b:
            b["series"] = ""

        pickled_books.update({b["_id"]: b})
    print("pickled_books updated in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    with open("unitedstates/{}".format(library_uuid), "wb") as f:
        pickle.dump(pickled_books, f)
    print("pickled_books dumped in {} seconds.".format(round(time.time() - t, 3)))

    # print("index/url written in {} seconds.".format(round(time.time() - t, 3)))
    return True


class Books(HTTPMethodView):
    def get(self, request, verb, library_uuid):
        assert request.stream is None
        return text("Here you should upload, no?")

    @stream_decorator
    async def post(self, request, verb, library_uuid):
        t = time.time()
        library_secret = request.headers.get("Library-Secret") or request.headers.get(
            "library-secret"
        )
        encoding_header = request.headers.get(
            "library-encoding"
        ) or request.headers.get("Library-Encoding")
        enc_zlib = False
        if encoding_header == "zlib":
            enc_zlib = True

        check_library_secret(library_uuid, library_secret)

        assert isinstance(request.stream, asyncio.Queue)
        result = b""
        while True:
            body = await request.stream.get()
            if body is None:
                print("books uploaded in {} seconds.".format(round(time.time() - t, 3)))
                if verb == "add":
                    t = time.time()
                    bookson = await validate_books(
                        result, motw.collection_schema, enc_zlib
                    )
                    print(
                        "books validated in {} seconds.".format(
                            round(time.time() - t, 3)
                        )
                    )
                    if bookson:
                        t = time.time()
                        if await add_books(bookson, library_uuid):
                            print(
                                "books added in {} seconds.".format(
                                    round(time.time() - t, 3)
                                )
                            )
                            return text("Books added...")
                elif verb == "remove":
                    bookson = await validate_books(result, motw.remove_schema, enc_zlib)

                    if bookson:
                        if await remove_books(bookson, library_uuid):
                            return text("Books removed...")

                abort(422, "{}-ing books failed!".format(verb))
            result += body


@app.route("/library/<verb>/<library_uuid>")
def library(request, verb, library_uuid):
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )
    r = re.match(
        "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}",
        library_secret,
    )
    if not r or verb not in ["add", "remove", "bookids", "on", "off"]:
        abort(422, "Wrong verb, ha!")

    if verb == "add":
        if library_uuid not in motw.libraries:
            motw.libraries.update(
                {library_uuid: {"secret": library_secret, "state": "off", "url": ""}}
            )
            with open("unitedstates/{}".format(library_uuid), "wb") as f:
                pickle.dump({}, f)
            motw.dump_libraries(motw.libraries)
            return text("{} added. Let's share books...".format(library_uuid))
        else:
            abort(422, "Library already added.")

    if not check_library_secret(library_uuid, library_secret):
        abort(401)

    if verb == "remove" and library_uuid in motw.libraries:
        with open("unitedstates/{}".format(library_uuid), "rb") as f:
            if len(pickle.load(f)) != 0:
                abort(
                    422,
                    "{} has still some books. Remove them first.".format(library_uuid),
                )
        del motw.libraries[library_uuid]
        motw.dump_libraries(motw.libraries)
        return text("{} removed.".format(library_uuid))

    elif verb == "on" and library_uuid in motw.libraries:
        t = time.time()
        if (
            "url" in request.args
            and request.args["url"][0] != motw.libraries[library_uuid]["url"]
        ):
            motw.libraries[library_uuid]["url"] = request.args["url"][0]
        elif (
            motw.libraries[library_uuid]["state"] == "on"
            and request.args["url"][0] != motw.libraries[library_uuid]["url"]
        ):
            abort(422, "{} is already online.".format(library_uuid))

        with open("unitedstates/{}".format(library_uuid), "rb") as f:
            motw.books.update(pickle.load(f))
        print("library ON in {} seconds.".format(round(time.time() - t, 3)))
        for b in motw.books.values():
            motw.indexed_by_time.update({str(b["last_modified"]) + b["_id"]: b["_id"]})
            motw.indexed_by_title.update({b["title_sort"] + b["_id"]: b["_id"]})
            motw.indexed_by_pubdate.update({str(b["pubdate"]) + b["_id"]: b["_id"]})
        motw.libraries[library_uuid]["state"] = "on"
        motw.dump_libraries(motw.libraries)
        return text("{} is back online.".format(library_uuid))

    elif verb == "off" and library_uuid in motw.libraries:
        t = time.time()
        if motw.libraries[library_uuid]["state"] == "off":
            abort(422, "{} is not online.".format(library_uuid))

        bookids = []
        for b in motw.books.values():
            if b["library_uuid"] == library_uuid:
                bookid = b["_id"]
                bookids.append(bookid)
                book = motw.books[bookid]
                motw.indexed_by_time.pop(
                    "{}{}".format(book["last_modified"], bookid), None
                )
                motw.indexed_by_title.pop(
                    "{}{}".format(book["title_sort"], bookid), None
                )
                motw.indexed_by_pubdate.pop(
                    "{}{}".format(book["pubdate"], bookid), None
                )
        for bid in bookids:
            del motw.books[bid]

        motw.libraries[library_uuid]["state"] = "off"
        motw.dump_libraries(motw.libraries)
        print("library OFF in {} seconds.".format(round(time.time() - t, 3)))
        return text("{} is now offline.".format(library_uuid))

    elif verb == "bookids" and library_uuid in motw.libraries:
        bookids = [
            "{}___{}".format(book["_id"], book["last_modified"])
            for book in motw.books.values()
            if library_uuid == book["library_uuid"]
        ]
        if bookids == []:
            try:
                with (open("unitedstates/{}".format(library_uuid), "rb")) as f:
                    bookids = [
                        "{}___{}".format(book["_id"], book["last_modified"])
                        for book in pickle.load(f).values()
                    ]
            except Exception as e:
                print("No cache + {}".format(e))
                bookids = []
        return json(bookids)
    elif library_uuid not in motw.libraries:
        abort(404, "{} doesn't exist.".format(library_uuid))


@app.route("/memory")
async def get_memory(request):
    proc = psutil.Process(os.getpid())
    import ipdb

    ipdb.set_trace()
    return json(
        {"memory": "{}".format(round(proc.memory_full_info().rss / 1000000.0, 2))}
    )


@app.route("/search/<field>/<q>")
def search(request, field, q):
    try:
        title = "search in {}".format(field)
        if field in [
            "_id",
            "title",
            "titles",
            "publisher",
            "series",
            "library_uuid",
            "abstract",
            "librarian",
        ]:
            if field == "titles":
                field = "title"
            h = hateoas(
                [b for b in motw.books.values() if unq(q).lower() in b[field].lower()],
                request,
                title,
            )
            return rs.raw(h, content_type="application/json")

        elif field in ["authors", "languages", "tags", ""]:
            h = hateoas(
                [
                    b
                    for b in motw.books.values()
                    if unq(q).lower() in " ".join(b[field]).lower()
                ],
                request,
                title,
            )
            return rs.raw(h, content_type="application/json")

    except Exception as e:
        abort(404, e)


@app.route("/autocomplete/<field>/<sq>")
def autocomplete(request, field, sq):
    r = set()
    if len(sq) < 4:
        return json({})
    if field in ["titles", "publisher"]:
        if field == "titles":
            field = "title"
            if unq(sq.lower()) in ["the "]:
                return json({})
        r = [
            b[field] for b in motw.books.values() if unq(sq.lower()) in b[field].lower()
        ]
    elif field in ["authors", "tags"]:
        for bf in [b[field] for b in motw.books.values()]:
            for f in bf:
                if unq(sq.lower()) in f.lower():
                    r.add(f)
    r = {"_items": list(r)}
    return json(r)


@app.route("/books")
def books(request):
    # h = hateoas([b for b in motw.books.values()],
    t = time.time()
    h = hateoas(
        [motw.books[bid] for bid in reversed(motw.indexed_by_time.values())], request
    )
    print("get books in {} seconds.".format(round(time.time() - t, 3)))
    return rs.raw(h, content_type="application/json")


@app.route("/book/<book_id>")
def book(request, book_id):
    try:
        return rs.raw(
            rjson.dumps(
                add_library_url(motw.books[book_id]), datetime_mode=rjson.DM_ISO8601
            ).encode(),
            content_type="application/json",
        )
    except Exception as e:
        abort(404, e)


app.add_route(Books.as_view(), "/books/<verb>/<library_uuid>")

asyncio.set_event_loop(uvloop.new_event_loop())
server = app.create_server(host="0.0.0.0", port=sys.argv[1])
loop = asyncio.get_event_loop()
task = asyncio.ensure_future(server)

signal(SIGINT, lambda s, f: loop.stop())

try:
    loop.run_forever()
except:
    loop.stop()
